const vaccine = require('./vaccine');
const constants = require('./constants');

var chennaiVaccine = new vaccine({
  ...constants.DISTRICTS.CHENNAI
});
chennaiVaccine.scheduleCron();

var bangaloreVaccine = new vaccine({
  ...constants.DISTRICTS.BENGALURU
});
bangaloreVaccine.scheduleCron();

var maduraiVaccine = new vaccine({
  ...constants.DISTRICTS.MADURAI
});
maduraiVaccine.scheduleCron();

var dindigulVaccine = new vaccine({
  ...constants.DISTRICTS.DINDIGUL
});
dindigulVaccine.scheduleCron();

var trichyVaccine = new vaccine({
  ...constants.DISTRICTS.TRICHY
});
trichyVaccine.scheduleCron();
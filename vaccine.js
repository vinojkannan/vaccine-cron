const axios = require('axios');
const moment = require('moment');
const _ = require('lodash');
const cron = require('node-cron');

class Vaccine {
  constructor(options) {
    this.areas = options.areas;
    this.chat_id = options.chat_id;
    this.cron = options.cron || '*/10 * * * * *';
    this.name = options.name;
  }
  sendMessage(payload) {
    console.log(`##${this.name}## ` + "Sending at " + new Date())
    axios.post(`https://api.telegram.org/bot1702816165:AAHK_lFZAr8sl4BIdoalvczt-xgI9srQSbI/sendMessage`, payload).then(response => {
      return response.data
    }).catch(err => {
      console.log('error', err.response.data)
    })
  }

  getCalendar(id) {
    return axios.get(`https://cdn-api.co-vin.in/api/v2/appointment/sessions/public/calendarByDistrict?district_id=${id}&date=${moment().format('DD-MM-YYYY')}`, {
      headers: {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36'
      }
    });
  }

  async constructAndSendMessage(centers) {
    let centerChunks = _.chunk(centers, 10);
    _.each(centerChunks, async (centers) => {
      if (_.size(centers) > 0) {
        centers = centers.map((center, index) => {
          return `${index+1}. ${center.name} \n${center.block_name} ${center.district_name}\nPincode: ${center.pincode}\nVaccine: ${center.vaccine}\nDose1 Capacity: ${center.capacity}\n`
        })
        let message = "Vaccination centers available for 18+ this week:\n" + centers.join("");
        console.log('message', message)
        await this.sendMessage({ chat_id: this.chat_id, text: message});
      }
    })
  }

  async checkAvailability(id) {
    let calendar = await this.getCalendar(id);
    let centers = [];
    _.each(calendar.data.centers, (center) => {
      let available = false;
      let vaccines = [], capacity = [];
      _.each(center.sessions, (session) => {
        if(session.min_age_limit == 18) {
          if (session.available_capacity_dose1 > 0) {
            available = true;
          }
          vaccines.push(session.vaccine);
          capacity.push(session.available_capacity_dose1);
        }
      })
      if (available) {
        centers.push({
          name: center.name,
          pincode: center.pincode,
          vaccine: _.uniq(vaccines),
          capacity: _.sum(capacity),
          district_name: center.district_name,
          block_name: center.block_name,
          pincode: center.pincode
        })
      }
    })
    await this.constructAndSendMessage(centers)
  }

  async scheduleCron() {
    cron.schedule(this.cron, async () => {
      try {
        console.log(`##${this.name}## ` + "Running at " + new Date())
        _.each(this.areas, async (area) => {
          await this.checkAvailability(area);
        })
      } catch (err) {
        console.log('error', err)
      }
    })
  }
}

module.exports = Vaccine;
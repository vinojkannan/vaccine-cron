const vaccine = require('./vaccine');
const constants = require('./constants');

var dindigulVaccine = new vaccine({
  ...constants.DISTRICTS.DINDIGUL
});
dindigulVaccine.scheduleCron();
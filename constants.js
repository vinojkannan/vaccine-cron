
const constants = {
  DISTRICTS: { /* don't expose password or any sensitive info, done only for demo */
    CHENNAI: {
      name: 'CHENNAI',
      areas: ['571'],
      chat_id: '-1001257726079',
      cron: '*/2 * * * *'
    },
    BENGALURU: {
      name: 'BENGALURU',
      areas: ['265', '294'],
      chat_id: '-1001363005222',
      cron: '*/2 * * * *'
    }, /* Temporarily using madurai for thiruvaru */
    MADURAI: {
      name: 'THIRUVARUR',
      areas: ['574'],
      chat_id: '-1001246444291',
      cron: '*/1 * * * *'
    },
    DINDIGUL: {
      name: 'DINDIGUL',
      areas: ['556'],
      chat_id: '-1001256722796',
      cron: '*/5 * * * *'
    },
    TRICHY: {
      name: 'TRICHY',
      areas: ['560'],
      chat_id: '-1001490071929',
      cron: '*/5 * * * *'
    },
    TEST: {
      name: 'TEST',
      areas: ['265', '294'],
      chat_id: '-1001156177122',
      cron: '*/5 * * * * *'
    }
  }
};

module.exports = constants;